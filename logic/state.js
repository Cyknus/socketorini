exports.State = class {
    constructor(bturn=0, workers=[], buildings=false, domes=[], players=[], possibles=[]) {
        this.bturn = bturn;
        this.workers = workers;
        this.buildings = buildings || new Array(25).fill(0);
        this.domes = domes;
        this.players = players;
        this.possibles = possibles;
        this.action = false
        this.chosen = false
    }

    clean() {
        this.bturn = 0;
        this.workers = [];
        this.buildings = new Array(25).fill(0);
        this.domes = [];
        this.players = [];
        this.possibles = [];
        this.action = false
        this.chosen = false
    }

    isTaken(lot) {
        return this.workers.map((w) => { return Number(w.id) }).includes(lot)
    }

    workerOfCurrentPlayer(id) {
        let is_it = false
        this.workers.forEach((w) => {
            if (w.id == id && w.p == this.bturn) {
              is_it = true;
            }
        })
        return is_it
    }

    setPossibleBuildsFor(id) {
        //move the worker first
        this.workers = this.workers.filter((w) => { return w.id != this.chosen })
        this.workers.push({id: id, p: this.bturn})

        const neighbors = this.getNeighbors(id)
        this.possibles = neighbors.filter(nghbr => this.isAvailable(nghbr))

        this.action = "build"
    }

    setPossibleMovesFor(id) {
        const neighbors = this.getNeighbors(id)
        this.possibles = neighbors.filter(nghbr => this.isAvailable(nghbr, id))
        this.action = "move"
        this.chosen = id
    }

    getNeighbors(id) {
        const corners = {
            0: [1,5,6],
            4: [3,8,9],
            20: [15, 16, 21],
            24: [18, 19, 23]
        }

        if (id in corners) {
            return corners[id]
        }

        if (id < 5) { // top row
            return [id-1, id+1, id+4, id+5, id+6]
        }

        if (id > 19) { // bottom row
            return [id-6, id-5, id-4, id-1, id+1]
        }

        if (id % 5 == 0) {
            return [id+1, id+5, id+6, id-5, id-4]
        }

        if ((id+1) % 5 == 0) {
            return [id-1, id+5, id+4, id-5, id-6]
        }

        return [id-6, id-5, id-4, id-1, id+1, id+4, id+5, id+6]
    }

    isAvailable(nghbr, worker=false) {
        let available = true

        available &= !(this.workers.map((w) => { return w.id }).indexOf(nghbr) >= 0)
        available &= !(this.domes.indexOf(nghbr) >= 0)

        if (worker && (this.buildings[nghbr] > 1)) {
            const currentFloor = this.buildings[worker]
            const target = this.buildings[nghbr]
            available &= target - currentFloor < 2
        }

        return available
    }

    putFloorOver(thisLot) {
        const height = this.buildings[thisLot] + 1
        if (height == 4) {
            this.domes.push(thisLot)
        }
        this.buildings[thisLot] = height
        this.action = false
        this.bturn = (this.bturn + 1) % 2
        this.possibles = []
    }

    isNotMyTurn(playerName) {
        return this.players.indexOf(playerName) != this.bturn
    }
}