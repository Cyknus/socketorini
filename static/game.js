const socket = io();
const BOARDSIZE = 5;

const actions = {
  "move": move,
  "build": build
}

const IAm = window.prompt("tu nombre:", "fulanito")

socket.emit('new player', IAm);
socket.on('player in', show);
socket.on('player out', alert);
socket.on('workersDone', workersDone);
socket.on('drawState', drawState);
socket.on('cleanAndDraw', cleanAndDraw);
socket.on('cleanAndBuild', cleanAndBuild);
socket.on('weHaveAWinner', gameOver);
socket.on('full', gameOver);


function show(state) {
  const turn = document.getElementById('turn');
  const msgs = ["Esperando al oponente", "Turno del oponente"];
  if (turn.innerText == msgs[0]) {
    turn.innerText = "Es tu turno"
  } else {
    const cTurn = state.players.indexOf(IAm)
    turn.innerText = msgs[cTurn]
  }
}

function showAction(a) {
  const messages = {
          'place': 'Fase de poner trabajadores',
          'choose': 'Fase de elegir un trabajador',
          'move': 'Fase de movimiento',
          'build': 'Fase de construccion'
  }
  const ad = document.getElementById('ad')
  ad.innerText = messages[a]
}


function drawBoard() {
  showAction('place')
  const board = document.getElementById('board')
  for (let i=0; i< BOARDSIZE; i++) {
    const row = document.createElement('div');
    for (let j=0; j< BOARDSIZE; j++) {
      const square = document.createElement('canvas');
      square.width = 100
      square.height = 100
      square.id = i*5+j;
      square.addEventListener('click', workerPlacement);
      row.appendChild(square);
    }
    board.appendChild(row);
  }
}

function drawState(state) {
  const turn = document.getElementById('turn');
  const cTurn = state.players[state.bturn]
  turn.innerText = cTurn == IAm ? "Es tu turno" : `Es turno de ${cTurn}`
  // draw possible moves
  whiteOut()
  rmListenerFrom([...Array(25).keys()], 'click', move)
  state.possibles.forEach((av) => {
    const possible = document.getElementById(av);
    possible.style.borderColor = 'green';
    if(state.action) {
      possible.addEventListener('click', actions[state.action])
      showAction(state.action)
    }
  })

  state.buildings.forEach((h, i) => {
    if(h != 0)
      drawHeight(Number(h), document.getElementById(i))
  })
  //for every worker in state, draw it
  state.workers.forEach((w) => {
    drawWorker(w.id, w.p)
  })
}

function move(e) {
  const lot = e.srcElement;
  socket.emit('mueveElBote', lot.id)
}

function drawWorker(w, n) {
  const lot = document.getElementById(w)
  if (lot.getContext) {
    const ctx = lot.getContext('2d');
    const worker = new Image();
    worker.onload = function () {
      ctx.drawImage(worker, 0, 0);
    }
    worker.src = `static/p${n}.png`
  }
}

function workersDone(workers) {
  showAction('choose')
  const alll = [...Array(25).keys()]
  rmListenerFrom(alll, 'click', workerPlacement);
  addEventListenersTo(workers.map((w) => {return w.id}), 'click', chooseWorker)
}

function workerPlacement(e) {
  const lot = e.srcElement;
  socket.emit('putWorkerOn', lot.id)
}

function chooseWorker(e) {
  const lot = e.srcElement;
  socket.emit('workerChosen', lot.id)
}

function rmWorkerFrom(lot_id) {
  const lot = document.getElementById(Number(lot_id))
  if (lot.getContext) {
    const ctx = lot.getContext('2d');
    ctx.clearRect(0, 0, 100, 100);
  }
}

function cleanAndDraw(state) {
  rmWorkerFrom(state.chosen)
  rmListenerFrom([...Array(25).keys()], 'click', move)
  rmListenerFrom([...Array(25).keys()], 'click', chooseWorker)
  drawState(state)
}

function build(e) {
  const lot = e.srcElement;
  socket.emit('buildIt', lot.id)
}


function cleanAndBuild(state) {
  showAction('choose')
  rmListenerFrom([...Array(25).keys()], 'click', build)
  addEventListenersTo(state.workers.map((w) => {return w.id}), 'click', chooseWorker)
  drawState(state)
}

function drawHeight(h, lot) {
  const floors = {
    1: fstFloor,
    2: sndFloor,
    3: thdFloor,
    4: dome
  }

  if (lot.getContext) {
    const ctx = lot.getContext('2d');
    floors[h](ctx)
  }
}

function fstFloor(ctx) {
  ctx.fillStyle = 'rgb(168, 219, 168)';
  ctx.fillRect(0, 0, 100, 100)
}

function sndFloor(ctx) {
  fstFloor(ctx)
  ctx.fillStyle = 'rgb(121, 189, 154)';
  ctx.fillRect(10, 10, 80, 80)
}

function thdFloor(ctx) {
  sndFloor(ctx)
  ctx.fillStyle = 'rgb(59, 134, 134)';
  ctx.fillRect(20, 20, 60, 60)
}

function dome(ctx) {
  thdFloor(ctx)
  ctx.fillStyle = 'rgb(11, 72, 107)';
  ctx.arc(50, 50, 30, 0, Math.PI*2, false)
  ctx.fill()
}

function whiteOut() {
  const cs = document.getElementsByTagName('canvas')
  for (let i = 0; i < BOARDSIZE*BOARDSIZE; i++) {
    cs[i].removeAttribute('style')
  }
}

function rmListenerFrom(lots, type, fn) {
  lots.forEach((lot) => {
    const c = document.getElementById(lot);
    c.removeEventListener(type, fn);
  })
}

function addEventListenersTo(idList, type, fn) {
  idList.forEach((id) => {
    const c = document.getElementById(id);
    c.addEventListener(type, fn)
  })
}

function gameOver(text) {
  const body = document.getElementsByTagName('body')
  const end = document.createElement('div')
  end.style.backgroundColor = "rgba(0,0,0,.65)"
  end.id = 'wall'
  const banner = document.createElement('div')
  banner.style.width = "50%";
  banner.style.margin = "150px auto"
  banner.id = 'banner'
  const msg = document.createElement('h1')
  msg.innerText = `${text} es el ganador`
  msg.id = 'winner'
  banner.append(msg)
  end.appendChild(banner)
  body[0].appendChild(end)
}

window.onload = drawBoard;
