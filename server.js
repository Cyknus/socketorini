const express = require('express');
const http = require('http');
const path = require('path');
const socketIO = require('socket.io');
const { State } = require('./logic/state.js')

const app = express();
const server = http.Server(app);
const io = socketIO(server);

app.set('port', 5000);
app.use('/static', express.static(__dirname + '/static'));

app.get('/', function(request, response) {
  response.sendFile(path.join(__dirname, 'index.html'));
});

server.listen(5000, function() {
  console.log("Listening on port 5000")
});

const state = new State();

io.on('connection', function(socket) {

  if (state.players.length == 2) {
     socket.emit('full', 'match is full')
  }

  let IAm;

  socket.on('new player', function(name) {
    IAm = name
    state.players.push(IAm)
    state.bturn = (state.bturn + 1) % 2
  	io.emit('player in', state);
  });

  socket.on('disconnect', function() {
    state.clean()
    io.emit('player out', `Partida terminada`);
  });

  socket.on('putWorkerOn', function(chosenLot) {
    if (state.isNotMyTurn(IAm)) { return; }

    const lot = Number(chosenLot)

    if (state.isTaken(lot)) { return; }

    state.workers.push({id: lot, p: state.bturn})

    if (state.workers.length == 4) {
        io.emit('workersDone', state.workers);
    }

    if ((state.workers.length % 2) == 0) {
        state.bturn = (state.bturn + 1) % 2
    }

    io.emit('drawState', state);
  });

  socket.on('workerChosen', function(chosenWorker) {
    if (state.isNotMyTurn(IAm)) { return; }

    const chosen = Number(chosenWorker)

    if (!state.workerOfCurrentPlayer(chosen)) { return; }

    state.setPossibleMovesFor(chosen)

    io.emit('drawState', state)
  });

  socket.on('mueveElBote', function(chosenLot) {
    if (state.isNotMyTurn(IAm)) { return; }

    const dest = Number(chosenLot)

    if (state.buildings[dest] == 3) {
      io.emit('weHaveAWinner', state.players[state.bturn])
      return;
    }

    state.setPossibleBuildsFor(dest)

    io.emit('cleanAndDraw', state)
  });

  socket.on('buildIt', function(chosenLot) {
    if (state.isNotMyTurn(IAm)) { return; }

    state.putFloorOver(Number(chosenLot))

    io.emit('cleanAndBuild', state)
  });
});

